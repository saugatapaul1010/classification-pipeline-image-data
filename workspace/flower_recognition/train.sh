python D:\\AI\\Classification-Pipeline\\utils\\data_preprocess.py --val_split 0.2 \
                                                                  --test_split 0.2 \
																                                  --data_dir D:/AI/Classification-Pipeline/data/classification_pipeline/checkbox_data/
python train_pipeline.py --model_name vgg16 \
                         --dense_neurons 1024 \
                         --batch_size 32 \
                         --stage1_lr 0.001 \
                         --stage2_lr 0.001 \
                         --monitor val_accuracy \
                         --metric accuracy \
                         --epochs1 1 \
                         --epochs2 1 \
                         --finetune yes \
                         --reduce_lr_patience 5 \
                         --reduce_lr_factor 0.05 \
                         --early_stop_patience 10 \
                         --model_chkpnt_period 1 --sim 50
