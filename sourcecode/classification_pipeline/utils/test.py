import glob
from PIL import Image
import os
from tqdm import tqdm
import pandas as pd
import glob
from sklearn.model_selection import train_test_split


source = "/home/developer/Desktop/Saugata/E-Crash/CA_Annotations/ALL_Partial_Annotation/"

files = [f for f in glob.glob(source + "/" + "**/*.csv", recursive=True)]
unique_labels = []

for file in files:
    df = pd.read_csv(file)
    lab = list(df.label)
    unique_labels.extend(lab)


unique_labels = list(set(unique_labels))

print(unique_labels)
