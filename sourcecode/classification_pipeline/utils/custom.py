#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 16 09:59:02 2020

@author: saugata paul
"""

#Import the deep learning libraries
from keras.preprocessing.image import ImageDataGenerator,load_img,img_to_array,array_to_img
from keras.models import Sequential, load_model
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense, BatchNormalization
from keras import backend as K
from keras.optimizers import RMSprop
from keras import regularizers as reg
import numpy as np
import matplotlib.pyplot as plt
import os
import imageio
from keras.layers.normalization import BatchNormalization
from keras import optimizers
from keras.callbacks import History

class CustomArchitecture:
    
    def __init__():
        pass
    
    def build_arch(self):
        
        #Dimensions of images to be fed to the model
        img_width, img_height = 128, 128
        
        if K.image_data_format() == 'channels_first':
            input_shape = (3, img_width, img_height)
        else:
            input_shape = (img_width, img_height, 3)

        model = Sequential()
        
        model = Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=input_shape, kernel_initializer='he_normal')(model)
        model = MaxPooling2D(pool_size=(2, 2))(model)
        
        model = Conv2D(64, kernel_size=(3, 3), activation='relu', kernel_initializer='he_normal')(model)
        model = MaxPooling2D(pool_size=(2, 2))(model)
        
        model = Conv2D(64, kernel_size=(3, 3), activation='relu', kernel_initializer='he_normal')(model)
        model = MaxPooling2D(pool_size=(2, 2))(model)
        
        model = Flatten()(model)
        
        model = Dense(512, activation='relu', kernel_initializer='he_uniform')(model)
        model = Dropout(rate=0.3)(model)
        
        model = Dense(3, activation='softmax',kernel_initializer='glorot_uniform')(model)
        
        optim1=optimizers.RMSprop(lr=1e-4,decay=0.001)
        
        model.compile(loss='categorical_crossentropy',
                      optimizer=optim1,
                      metrics=['accuracy'])
        