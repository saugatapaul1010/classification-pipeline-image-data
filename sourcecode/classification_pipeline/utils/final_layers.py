#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 30 16:12:22 2020

@author: saugata paul
"""

from keras.layers import Dense

class Final_Layers:

    def __init__(self, input_params, layer):
        self.input_params = input_params
        self.layer = layer

    def create_layers(self):
        model = Dense(self.input_params['dense_neurons'], activation='relu', kernel_initializer='he_normal')(self.layer)
        #model = Dense(self.input_params['dense_neurons'], activation='relu', kernel_initializer='he_normal')(model)
        #model = Dense(self.input_params['dense_neurons'], activation='relu', kernel_initializer='he_normal')(model)
        print("Creating the final dense layers... Successfull !")
        return model
