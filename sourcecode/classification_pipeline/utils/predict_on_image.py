# -*- coding: utf-8 -*-
"""
Created on Thu Feb 27 02:35:28 2020

@author: sauga
"""

import pandas as pd
from keras.models import load_model
from keras.applications.vgg16 import preprocess_input as preprocess_input_vgg16
from PIL import Image
from keras.preprocessing.image import load_img
import cv2
from keras_preprocessing.image import ImageDataGenerator
import os
import numpy as np
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
from keras.preprocessing.image import img_to_array

root_path = "D:/AI/Classification-Pipeline/"
data_path = "C:/Users/sauga/Downloads/Data_Check_Unchecked/Checked/"
image_path = [data_path + f for f in os.listdir(data_path)]
model = load_model(root_path + "simulations/SIM_100/training_results/stage2/vgg16_model_stage_2.h5")

image = cv2.imread(image_path[0])

label_map=dict({"Checked":0,
                "Unchecked":1})


image = load_img(image_path[0])
image = img_to_array(image)
image = image.reshape((1, image.shape[0], image.shape[1], image.shape[2]))000
image = preprocess_input_vgg16(image)

y_pred = model.predict(image)





def interpret(text):
    return label_map[text]

data = pd.read_msgpack("C:/Users/sauga/Downloads/"+"data_df/dataframe.msgpack")
data["y_true"] = data["class_label"].apply(lambda x:interpret(str(x)))

test_datagen = ImageDataGenerator(preprocessing_function=preprocess_input_vgg16)

test_generator = test_datagen.flow_from_dataframe(dataframe=data,
                                                  directory="C:/Users/sauga/Downloads/data/",
                                                  target_size=(224,224),
                                                  x_col="filenames",
                                                  y_col="class_label",
                                                  batch_size=1,
                                                  class_mode='categorical',
                                                  color_mode='rgb',
                                                  shuffle=False)

nb_test_samples = len(test_generator.classes)

#Predictions (Probability Scores and Class labels)
y_pred_proba = model.predict_generator(test_generator, nb_test_samples // 1)
y_pred = np.argmax(y_pred_proba, axis=1)

data['pred_label'] = y_pred
data.to_csv("C:/Users/sauga/Downloads/eval.csv", index=None)

